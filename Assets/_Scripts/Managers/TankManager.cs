﻿using System;
using UnityEngine;

[Serializable]
public class TankManager
{
    public Color PlayerColor;            
    public Transform SpawnPoint;         
    [HideInInspector]
    public int PlayerNumber;             
    [HideInInspector]
    public string ColoredPlayerText;
    [HideInInspector]
    public GameObject Instance;          
    [HideInInspector]
    public int Wins;                     


    private TankMovement movement;       
    private TankShooting shooting;
    private GameObject canvasGameObject;


    public void Setup()
    {
        movement = Instance.GetComponent<TankMovement>();
        shooting = Instance.GetComponent<TankShooting>();
        canvasGameObject = Instance.GetComponentInChildren<Canvas>().gameObject;

        movement.PlayerNumber = PlayerNumber;
        shooting.PlayerNumber = PlayerNumber;

        ColoredPlayerText = "<color=#" + ColorUtility.ToHtmlStringRGB(PlayerColor) + ">PLAYER " + PlayerNumber + "</color>";

        MeshRenderer[] renderers = Instance.GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.color = PlayerColor;
        }
    }


    public void DisableControl()
    {
        movement.enabled = false;
        shooting.enabled = false;

        canvasGameObject.SetActive(false);
    }


    public void EnableControl()
    {
        movement.enabled = true;
        shooting.enabled = true;

        canvasGameObject.SetActive(true);
    }


    public void Reset()
    {
        Instance.transform.position = SpawnPoint.position;
        Instance.transform.rotation = SpawnPoint.rotation;

        Instance.SetActive(false);
        Instance.SetActive(true);
    }
}
