﻿using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float DampTime = 0.2f;                 
    public float ScreenEdgeBuffer = 4f;           
    public float MinSize = 7f;                  
    [HideInInspector]
    public Transform[] Targets; 

    private Camera camera;                        
    private float zoomSpeed;                      
    private Vector3 moveVelocity;                 
    private Vector3 desiredPosition;              


    private void Awake()
    {
        camera = GetComponentInChildren<Camera>();
    }


    private void FixedUpdate()
    {
        Move();
        Zoom();
    }


    /// <summary>
    /// Move the camera to an average position between all tanks
    /// </summary>
    private void Move()
    {
        FindAveragePosition();

        transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref moveVelocity, DampTime);
    }


    /// <summary>
    /// Find average position between the two tanks
    /// </summary>
    private void FindAveragePosition()
    {
        Vector3 averagePos = new Vector3();
        int numTargets = 0;

        for (int i = 0; i < Targets.Length; i++)
        {
            // Unactive tanks are not included in calculations (ie. dead tanks)
            if (!Targets[i].gameObject.activeSelf) continue;

            averagePos += Targets[i].position;
            numTargets++;
        }

        if (numTargets > 0)
        {
            averagePos /= numTargets;
        }

        // Camera "y" position should not change
        averagePos.y = transform.position.y;

        desiredPosition = averagePos;
    }


    /// <summary>
    /// Zoom the camera to display all tanks
    /// </summary>
    private void Zoom()
    {
        float requiredSize = FindRequiredSize();
        camera.orthographicSize = Mathf.SmoothDamp(camera.orthographicSize, requiredSize, ref zoomSpeed, DampTime);
    }


    /// <summary>
    /// Find the required size (zoom) for the camera based on
    /// </summary>
    /// <returns></returns>
    private float FindRequiredSize()
    {
        // Transform local space to world space
        Vector3 desiredLocalPos = transform.InverseTransformPoint(desiredPosition);

        float size = 0f;

        for (int i = 0; i < Targets.Length; i++)
        {
            // Unactive tanks are not included in calculations (ie. dead tanks)
            if (!Targets[i].gameObject.activeSelf) continue;

            // Transform local space to world space
            Vector3 targetLocalPos = transform.InverseTransformPoint(Targets[i].position);

            Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

            // Choose largest size (vertical vs horizontal) that will accommodate all tanks
            size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.y));
            size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.x) / camera.aspect);
        }
        
        // Prevent tanks from disappearing near screen edge
        size += ScreenEdgeBuffer;

        // Prevent zooming in too far
        size = Mathf.Max(size, MinSize);

        return size;
    }


    /// <summary>
    /// Set the camera's starting position and zoom
    /// </summary>
    public void SetStartPositionAndSize()
    {
        FindAveragePosition();

        transform.position = desiredPosition;

        camera.orthographicSize = FindRequiredSize();
    }
}