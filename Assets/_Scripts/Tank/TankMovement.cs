﻿using UnityEngine;

public class TankMovement : MonoBehaviour
{
    public int PlayerNumber = 1;
    public float Speed = 12f;
    public float TurnSpeed = 180f;
    public AudioSource MovementAudio;
    public AudioClip EngineIdling;
    public AudioClip EngineDriving;
    public float PitchRange = 0.2f;

    private string movementAxisName;
    private string turnAxisName;
    private Rigidbody rigidbody;
    private float movementInputValue;
    private float turnInputValue;
    private float originalPitch;


    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }


    private void OnEnable ()
    {
        rigidbody.isKinematic = false;
        movementInputValue = 0f;
        turnInputValue = 0f;
    }


    private void OnDisable ()
    {
        rigidbody.isKinematic = true;
    }


    private void Start()
    {
        movementAxisName = "Vertical" + PlayerNumber;
        turnAxisName = "Horizontal" + PlayerNumber;

        originalPitch = MovementAudio.pitch;
    }

    private void Update()
    {
        // Store the player's input and make sure the audio for the engine is playing.
        movementInputValue = Input.GetAxis(movementAxisName);
        turnInputValue = Input.GetAxis(turnAxisName);

        EngineAudio();
    }


    private void FixedUpdate()
    {
        // Move and turn the tank.
        Move();
        Turn();
    }


    private void EngineAudio()
    {
        // Play the correct audio clip based on whether or not the tank is moving and what audio is currently playing.
        bool isMoving = Mathf.Abs(movementInputValue) > 0.1f || Mathf.Abs(turnInputValue) > 0.1f;

        // Audio changes clip and pitch only when moving from "moving" to "not moving"
        if (isMoving)
        {
            if (MovementAudio.clip == EngineIdling)
            {
                MovementAudio.clip = EngineDriving;
                MovementAudio.pitch = Random.Range(originalPitch - PitchRange, originalPitch + PitchRange);
                MovementAudio.Play();
            }
        }
        else
        {
            if (MovementAudio.clip == EngineDriving)
            {
                MovementAudio.clip = EngineIdling;
                MovementAudio.pitch = Random.Range(originalPitch - PitchRange, originalPitch + PitchRange);
                MovementAudio.Play();
            }
        }
    }


    private void Move()
    {
        // Adjust the position of the tank based on the player's input.
        Vector3 movement = transform.forward * movementInputValue * Speed * Time.deltaTime;
        rigidbody.MovePosition(rigidbody.position + movement);
    }


    private void Turn()
    {
        // Adjust the rotation of the tank based on the player's input.
         float turnAngle = turnInputValue * TurnSpeed * Time.deltaTime;

         Quaternion turnRotation = Quaternion.Euler(0,turnAngle, 0);
         rigidbody.MoveRotation(rigidbody.rotation * turnRotation);
    }
}