﻿using UnityEngine;
using UnityEngine.UI;

public class TankShooting : MonoBehaviour
{
    public int PlayerNumber = 1;       
    public Rigidbody Shell;            
    public Transform FireTransform;    
    public Slider AimSlider;           
    public AudioSource ShootingAudio;  
    public AudioClip ChargingClip;     
    public AudioClip FireClip;         
    public float MinLaunchForce = 15f; 
    public float MaxLaunchForce = 30f; 
    public float MaxChargeTime = 0.75f;

    private string fireButton;         
    private float currentLaunchForce;  
    private float chargeSpeed;         
    private bool fired;


    private void OnEnable()
    {
        currentLaunchForce = MinLaunchForce;
        AimSlider.value = MinLaunchForce;
    }


    private void Start()
    {
        fireButton = "Fire" + PlayerNumber;

        chargeSpeed = (MaxLaunchForce - MinLaunchForce) / MaxChargeTime;

        AimSlider.minValue = MinLaunchForce;
        AimSlider.maxValue = MaxLaunchForce;
    }


    private void Update()
    {
        // Track the current state of the fire button and make decisions based on the current launch force.
        AimSlider.value = MinLaunchForce;

        // Button has been pressed past the maximumum charge time
        if (currentLaunchForce >= MaxLaunchForce && !fired)
        {
            currentLaunchForce = MaxLaunchForce;

            Fire();
        }
        // Button has just been pressed
        else if (Input.GetButtonDown(fireButton))
        {
            fired = false;
            currentLaunchForce = MinLaunchForce;

            ShootingAudio.clip = ChargingClip;
            ShootingAudio.Play();
        }
        // Button is currently held
        else if (Input.GetButton(fireButton) && !fired)
        {
            currentLaunchForce += chargeSpeed * Time.deltaTime;

            AimSlider.value = currentLaunchForce;
        }
        // Button has just been released
        else if (Input.GetButtonUp(fireButton) && !fired)
        {
            Fire();
        }
    }


    /// <summary>
    /// Launch shell
    /// </summary>
    private void Fire()
    {
        fired = false;

        Rigidbody shellInstance = Instantiate(Shell, FireTransform.position, FireTransform.rotation) as Rigidbody;
        shellInstance.velocity = currentLaunchForce * FireTransform.forward;

        ShootingAudio.clip = FireClip;
        ShootingAudio.Play();

        currentLaunchForce = MinLaunchForce;
    }
}