using UnityEngine;

public class UIDirectionControl : MonoBehaviour
{
    public bool UseRelativeRotation = true;  

    private Quaternion relativeRotation;     


    private void Start()
    {
        relativeRotation = transform.parent.localRotation;
    }


    private void Update()
    {
        // Prevent UI from rotating with the tank
        if (UseRelativeRotation)
        {
            transform.rotation = relativeRotation;
        }
    }
}
